//
//  ViewController.h
//  ScrollTweakOne
//
//  Created by Javier Alonso Gutiérrez on 28/04/12.
//  Copyright (c) 2012 NG Servicios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *menuScrollView;

@end
