//
//  JAFirstTweakScrollView.m
//  ScrollTweakOne
//
//  Created by Javier Alonso Gutiérrez on 28/04/12.
//  Copyright (c) 2012 NG Servicios. All rights reserved.
//

#import "JAFirstTweakScrollView.h"

@implementation JAFirstTweakScrollView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    for (UIView *subview in self.subviews) {
        if ([subview pointInside:[self convertPoint:point toView:subview] withEvent:event]) {
            return YES;
        }
    }
    return NO;
}

@end
