//
//  ViewController.m
//  ScrollTweakOne
//
//  Created by Javier Alonso Gutiérrez on 28/04/12.
//  Copyright (c) 2012 NG Servicios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize menuScrollView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.menuScrollView.contentSize=CGSizeMake(self.menuScrollView.bounds.size.width, 1040);
}

- (void)viewDidUnload
{
    [self setMenuScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
